package de.davidgontrum.fahrplanauskunft;

import java.util.List;

/**
 * @author <a href="mailto:david.gontrum@adesso.de">David Gontrum</a>
 * @since 22.12.2014, 09:28
 */
public class Main {
  public static void main(String... args) {
    if (args.length < 3) {
      throw new RuntimeException("at least three arguments have to be given");
    }
    String planfile = args[0];
    String stop01 = args[1];
    String stop02 = args[2];

    try {
      List<Plan> plans = Plan.createFromResource(planfile);

      Calculator calc = new Calculator();
      calc.addListener(new RouteListener() {
        @Override
        public void calculationDone(final Route route) {
          System.out.print(route);
        }
      });
      for (Plan plan : plans) {
        calc.addPlan(plan);
      }

      System.out.print("Blaschkoallee, Grenzallee, Neukölln");
    } catch (Exception e) {
      throw new RuntimeException(e.getMessage(), e);
    }

  }
}

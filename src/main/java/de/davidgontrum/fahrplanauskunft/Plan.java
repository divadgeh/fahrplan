package de.davidgontrum.fahrplanauskunft;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by obmib on 20.12.14.
 */
public class Plan {
  private List<Stop> stops = new LinkedList<Stop>();
  private String name;

  public int getIndexOfStation(Stop stop) {
    return stops.indexOf(stop);
  }

  public Plan reversePlan() {
    final ArrayList<Stop> reverseStops = new ArrayList<Stop>(stops);
    Collections.reverse(reverseStops);
    return Plan.createPlanFromStops(reverseStops);
  }

  private static Plan createPlanFromStops(final ArrayList<Stop> reverse) {
    Plan plan = new Plan();
    for (Stop stop : reverse) {
      plan.addStop(stop);
    }
    return plan;
  }

  public static List<Plan> createFromResource(final String plan) throws Exception {
    final InputStream planStream = Plan.class.getResourceAsStream(plan);

    BufferedReader streamReader = new BufferedReader(new InputStreamReader(planStream, "UTF-8"));
    StringBuilder responseStrBuilder = new StringBuilder();

    String inputStr;
    while ((inputStr = streamReader.readLine()) != null) {
      responseStrBuilder.append(inputStr);
    }


    //FileReader reader = new FileReader(plan);
    JSONParser jsonParser = new JSONParser();
    JSONObject jsonObject = (JSONObject) jsonParser.parse(responseStrBuilder.toString());
    final JSONArray plans = (JSONArray) jsonObject.get("Plans");
    List<Plan> resultPlans = new ArrayList<Plan>();
    for (Object planObject : plans) {
      JSONObject planJSON = (JSONObject) planObject;
      resultPlans.add(Plan.createPlanFromJSONObject(planJSON));
    }
    return resultPlans;

  }

  private static Plan createPlanFromJSONObject(final JSONObject planJSON) {
    Plan plan = new Plan();
    final String name = (String) planJSON.get("Name");
    final JSONArray stops = (JSONArray) planJSON.get("Stops");

    plan.setName(name);
    for (int i = 0; i < stops.size(); i++) {
      final String stopName = (String) ((JSONObject) stops.get(i)).get("Name");
      Stop stop = new Stop(stopName);
      plan.addStop(stop);
    }

    return plan;
  }

  public static Plan createPlanFromString(final String planString) throws ParseException {
    JSONParser jsonParser = new JSONParser();
    final JSONObject parsedPlan = (JSONObject) jsonParser.parse(planString);

    return createPlanFromJSONObject(parsedPlan);
  }

  private void addStop(final Stop stop) {
    this.stops.add(stop);
  }

  public String getStringRepresentation() {
    return this.toString();
  }

  public void setName(final String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "Plan{" +
        "stops=" + stops +
        ", name='" + name + '\'' +
        '}';
  }

  public Stop getStop(final int i) {
    return this.stops.get(i);
  }

  public Route getPartRoute(final int placeOfFirstStop, final int placeOfSecondStop) {
    return new Route(stops.subList(placeOfFirstStop, placeOfSecondStop + 1));
  }
}

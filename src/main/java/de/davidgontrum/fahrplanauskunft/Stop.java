package de.davidgontrum.fahrplanauskunft;

/**
 * Created by obmib on 20.12.14.
 */
public class Stop {
  private String name;

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Stop)) {
      return false;
    }

    final Stop stop = (Stop) o;

    if (name != null ? !name.equals(stop.name) : stop.name != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    return name != null ? name.hashCode() : 0;
  }

  public Stop(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}

package de.davidgontrum.fahrplanauskunft;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * @author <a href="mailto:david.gontrum@adesso.de">David Gontrum</a>
 * @since 22.12.2014, 12:14
 */
public class Route {
  List<Stop> stops = new LinkedList<Stop>();

  public Route(final Stop... stops) {
    iterateAndAddStops(Arrays.asList(stops));
  }

  public Route(final List<Stop> stops) {
    iterateAndAddStops(stops);
  }

  private void iterateAndAddStops(Iterable<Stop> stops) {
    for (Stop stop : stops) {
      this.stops.add(stop);
    }
  }

  public List<Stop> getStops() {
    return stops;
  }

  @Override
  public String toString() {
    StringBuffer sb = new StringBuffer();
    for (Stop stop : stops) {
      if (sb.length() != 0) {
        sb.append(", ");
      }
      sb.append(stop.getName());
    }
    return sb.toString();
  }
}

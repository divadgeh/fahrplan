package de.davidgontrum.fahrplanauskunft;

/**
 * @author <a href="mailto:david.gontrum@adesso.de">David Gontrum</a>
 * @since 22.12.2014, 12:19
 */
public interface RouteListener {
  void calculationDone(Route route);
}

package de.davidgontrum.fahrplanauskunft;

import java.util.ArrayList;
import java.util.List;

public class Calculator {
  private List<Plan> plans = new ArrayList<Plan>();
  //private Plan plan;
  private List<RouteListener> calculationListener = new ArrayList<RouteListener>();

  public void calculateRoute(Stop stop1, Stop stop2) {
    Plan plan = plans.get(0);
    RouteValues routeValues = new RouteValues(plan.getIndexOfStation(stop1), plan.getIndexOfStation(stop2));
    if (plan.getIndexOfStation(stop1) > plan.getIndexOfStation(stop2)) {
      plan = plan.reversePlan();
    }

    Route result = plan.getPartRoute(routeValues.start(), routeValues.end());

    for (RouteListener listener : calculationListener) {
      listener.calculationDone(result);
    }
  }

  public void addListener(RouteListener listener) {
    calculationListener.add(listener);
  }


  public void addPlan(final Plan plan) {
    plans.add(plan);
  }
}
package de.davidgontrum.fahrplanauskunft;

/**
 * @author <a href="mailto:david.gontrum@adesso.de">David Gontrum</a>
 * @since 22.12.2014, 14:36
 */
public class RouteValues {
  private int[] values = new int[2];

  public RouteValues(int start, int end) {
    values[0] = start;
    values[1] = end;
  }

  public int start() {
    return Math.min(values[0], values[1]);
  }

  public int end() {
    return Math.max(values[0], values[1]);
  }
}

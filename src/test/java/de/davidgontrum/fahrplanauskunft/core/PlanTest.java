package de.davidgontrum.fahrplanauskunft.core;

import de.davidgontrum.fahrplanauskunft.Plan;
import de.davidgontrum.fahrplanauskunft.Route;
import de.davidgontrum.fahrplanauskunft.RouteListener;
import de.davidgontrum.fahrplanauskunft.Stop;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class PlanTest {

  Plan plan;
  private String planString = "{\"Name\": \"U1\",\"Stops\": [{\"Name\": \"Kottbusser Tor\"},{\"Name\": \"Görlitzer Bahnhof\"},{\"Name\": \"Schlesisches Tor\"},{\"Name\": \"Warschauer Straße\"}]}";


  @Before
  public void setUp() throws Exception {
    plan = Plan.createPlanFromString(planString);
  }

  @Test
  public void makesSureThatHaltsAreOrdered() {
    assertThat(plan.getStop(0).getName(), is("Kottbusser Tor"));
    assertThat(plan.getStop(1).getName(), is("Görlitzer Bahnhof"));
    assertThat(plan.getStop(2).getName(), is("Schlesisches Tor"));
  }

}
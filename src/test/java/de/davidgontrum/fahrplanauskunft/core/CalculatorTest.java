package de.davidgontrum.fahrplanauskunft.core;

import de.davidgontrum.fahrplanauskunft.*;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.verify;

/**
 * @author <a href="mailto:david.gontrum@adesso.de">David Gontrum</a>
 * @since 22.12.2014, 14:42
 */
@RunWith(MockitoJUnitRunner.class)
public class CalculatorTest {

  @Mock
  private RouteListener listener;

  private Plan plan;
  private String planString = "{\"Name\": \"U1\",\"Stops\": [{\"Name\": \"Kottbusser Tor\"},{\"Name\": \"Görlitzer Bahnhof\"},{\"Name\": \"Schlesisches Tor\"},{\"Name\": \"Warschauer Straße\"}]}";

  private Calculator calculator;

  @Before
  public void setUp() throws Exception {
    plan = Plan.createPlanFromString(planString);
    calculator = new Calculator();
    calculator.addPlan(plan);
    calculator.addListener(listener);
  }

  @Test
  public void calculationWillCallListener() {
    calculator.calculateRoute(new Stop("Kottbusser Tor"), new Stop("Warschauer Straße"));
    verify(listener).calculationDone(argThat(is(aRouteThatContains(new Stop("Kottbusser Tor"), new Stop("Görlitzer Bahnhof"), new Stop("Schlesisches Tor"), new Stop("Warschauer Straße")))));
  }

  private Matcher<Route> aRouteThatContains(final Stop... stops) {
    return new TypeSafeMatcher<Route>() {
      @Override
      protected boolean matchesSafely(Route item) {
        return item.getStops().containsAll(Arrays.asList(stops));
      }

      @Override
      public void describeTo(final Description description) {
        StringBuffer expected = new StringBuffer();
        for (Stop stop : stops) {
          if (expected.length() != 0) {
            expected.append(", ");
          }
          expected.append(stop.getName());
        }

        description.appendText("should contain").appendValue(expected.toString());
      }

      @Override
      public void describeMismatchSafely(final Route item, final Description description) {
        StringBuffer actual = new StringBuffer();
        for (Stop stop : item.getStops()) {
          if (actual.length() != 0) {
            actual.append(", ");
          }
          actual.append(stop.getName());
        }
        description.appendText("but was").appendValue(actual);
      }
    };


  }
}

package de.davidgontrum.fahrplanauskunft.endtoend;

import de.davidgontrum.fahrplanauskunft.ApplicationRunner;
import de.davidgontrum.fahrplanauskunft.Stop;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.LogMode;
import org.junit.contrib.java.lang.system.StandardOutputStreamLog;
import org.junit.experimental.categories.Category;

/**
 * @author <a href="mailto:david.gontrum@adesso.de">David Gontrum</a>
 * @since 22.12.2014, 08:44
 */
@Category(EndToEndCategory.class)
public class FahrplanEndToEndTest {

  private ApplicationRunner applicationRunner = new ApplicationRunner();

  @Rule
  public final StandardOutputStreamLog log = new StandardOutputStreamLog(LogMode.LOG_ONLY);

  @Test
  public void fahrplanReturnsRouteForTwoStops() {
    applicationRunner.addLogging(log);
    applicationRunner.addPlanForCalculation("testroute01.json");

    Stop blaschkoallee = new Stop("Blaschkoallee");
    Stop neukoelln = new Stop("Neukölln");
    Stop grenzallee = new Stop("Grenzallee");

    applicationRunner.addStops(blaschkoallee.getName(), neukoelln.getName());

    applicationRunner.showsRoute(blaschkoallee, grenzallee, neukoelln);
  }
}

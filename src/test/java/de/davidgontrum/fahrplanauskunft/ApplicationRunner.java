package de.davidgontrum.fahrplanauskunft;

import org.junit.contrib.java.lang.system.LogMode;
import org.junit.contrib.java.lang.system.StandardOutputStreamLog;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


/**
 * Created by obmib on 20.12.14.
 */
public class ApplicationRunner {

  String plan;
  public StandardOutputStreamLog log;

  public void addPlanForCalculation(String plan) {
    this.plan = plan;
  }

  public void addStops(final String stop1, final String stop2) {
    if (plan == null) {
      throw new RuntimeException("plan was not defined");
    }

    Main.main(plan, stop1, stop2);
  }

  public void showsRoute(final Stop... stops) {
    StringBuffer sb = new StringBuffer();
    for (Stop stop : stops) {
      if (sb.length() != 0) {
        sb.append(", ");
      }
      sb.append(stop.getName());
    }
    final String actual = log.getLog();

    assertThat(actual, is(sb.toString()));
  }

  public void addLogging(final StandardOutputStreamLog log) {
    this.log = log;
  }
}

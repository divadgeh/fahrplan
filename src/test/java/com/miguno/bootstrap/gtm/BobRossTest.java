package com.miguno.bootstrap.gtm;

import com.google.common.collect.ImmutableSet;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.testng.annotations.DataProvider;

import java.io.PrintStream;
import java.util.Set;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * This test class shows how to write unit tests with TestNG, Mockito and FEST-Assert 2.
 * 
 * Don't pay too much attention to the semantics of the actual tests.
 */
@RunWith(JUnit4.class)
public class BobRossTest {

    private static final Set<String> ANY_PAINTING_ELEMENTS = ImmutableSet.of("sky", "mountain", "happy tree");
    private static final Set<String> ANY_PAINTING_ELEMENTS_BUT_NO_HAPPY_TREE = ImmutableSet.of("sky", "mountain");

    /**
     * Shows using mocks
     */
    @Test
    public void shouldCommunicateWhenPainting() {
        // given
        PrintStream printStream = mock(PrintStream.class);
        BobRoss bob = new BobRoss(ANY_PAINTING_ELEMENTS, printStream);

        // when
        bob.paintPicture();

        // then
        verify(printStream, times(ANY_PAINTING_ELEMENTS.size())).println(any(String.class));
    }

    /**
     * Shows testing for expected exceptions
     */
    @Test(expected = UnsupportedOperationException.class)
    public void shouldReturnImmutableSetOfPaintingElements() {
        // given
        BobRoss example = new BobRoss(ANY_PAINTING_ELEMENTS);

        // when
        Set<String> paintingElements = example.getPaintingElements();

        // then
        paintingElements.add("happy little accident");
    }

    @DataProvider
    public Object[][] paintingElementsWithoutHappyTreeData() {
        return new Object[][] {
                { ImmutableSet.of("sky", "mountain", "cloud") },
                { ImmutableSet.of("sky", "lake", "barn", "squirrel") } };
    }


}
